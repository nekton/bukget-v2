package com.Richee.Search;

import com.Richee.Filter.Categorys;
import com.Richee.Filter.Sortby;

public class Filter {
	
	private Categorys category;
	private Sortby sortby;
	private String version;
	
	Categorys category() { return this.category; }
	Sortby sortby() { return this.sortby; }
	String version() { return this.version; }
	
	public Filter(Categorys category, Sortby sortby, String gameversion) {
		this.category = category;
		this.sortby = sortby;
		this.version = gameversion;
	}
	
	public void setFilter(Categorys category, Sortby sortby, String gameversion) {
		this.category = category;
		this.sortby = sortby;
		this.version = gameversion;
	}
}
