package com.Richee.Search;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Version {
	
	private String getFileLink(String link) {
		StringBuilder b = new StringBuilder();
		for(char c  : link.toCharArray()) {
			if(c=='?') break;
			b.append(c);
		}
		name = b.toString().substring("https://dev.bukkit.org/projects/".length());
		return b.append("/files").toString();
	}
	
	private String download, name;
	
	public Version(String plLink) throws Exception {
		String flink = this.getFileLink(plLink);
		
		Document doc = Jsoup.connect(flink).get();
		Element table = doc.select("table[class=listing listing-project-file project-file-listing b-table b-table-a]").first();
		Element tbody = table.select("tbody").first();
		Element latest = tbody.select("tr").first();
		Element downloadbt = latest.select("a[class=button tip fa-icon-download icon-only]").first();
		this.download = downloadbt.attr("abs:href");
	}
	
	public String download() { return this.download; }
	public String name() { return this.name; }
}
