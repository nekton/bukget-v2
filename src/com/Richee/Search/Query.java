package com.Richee.Search;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.Richee.Filter.GameVersion;
import com.Richee.Result.SearchResult;

public class Query {
	private String s = "";
	
	public Query(String search) {
		this.s = search;
	}
	
	public void setQuery(String search) {
		this.s = search;
	}
	
	public SearchResult search() {
		try {
			Document doc = Jsoup.connect("https://dev.bukkit.org/search?search=" + this.s).get();
			Elements es = doc.select("tr[class=results]");
			
			List<String> results = new ArrayList<String>();
			
			for(Element e : es) {
				Elements divs = e.select("div[class=results-name]");
				
				for(Element div : divs) {
					Element link = div.select("a[href]").first();
					
					results.add(link.attr("abs:href"));
				}
			}
			return new SearchResult(results);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public String searchFilter(Filter f, int p) {
		try {
			
			StringBuilder uriBuilder = new StringBuilder().append("https://dev.bukkit.org/bukkit-plugins"+f.category().getCategoryName()+"?");
			
			if(f.sortby()!=null) {
				uriBuilder.append("filter-sort=" + f.sortby().f());
			}
			if(f.version()!=null) {
				String v = GameVersion.getVersion(f.version());
				if(v!="") {
					uriBuilder.append("&filter-game-version=" + v);
				}
			}
			if(p>0) {
				uriBuilder.append("&page="+p);
			}
			
			Document doc = Jsoup.connect(uriBuilder.toString()).get();
			
			return "";
		} catch(Exception ex) {
			return null;
		}
	}
}
