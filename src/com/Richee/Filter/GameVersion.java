package com.Richee.Filter;

import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GameVersion {
	
	public static HashMap<String, String> getAllVersions() throws Exception {
		Document doc = Jsoup.connect("https://dev.bukkit.org/bukkit-plugins?filter-sort=name").get();
		Element select = doc.select("select[id=filter-game-version]").first();
		Elements children = select.children();
		
		HashMap<String, String> r = new HashMap<String, String>();
		
		for(Element child : children) {
			r.put(child.text().toLowerCase(), child.val());
		}
		
		return r;
	}
	public static String[] getAllVersionsAsArray() throws Exception {
		Document doc = Jsoup.connect("https://dev.bukkit.org/bukkit-plugins?filter-sort=name").get();
		Element select = doc.select("select[id=filter-game-version]").first();
		Elements children = select.children();
		
		String[] ar = new String[children.size()];
		
		int i = 0;
		for(Element child : children) {
			ar[i] = child.text();
			i++;
		}
		
		return ar;
	}
	
	private static HashMap<String, String> vals = new HashMap<String, String>();
	
	public static String getVersion(String version) throws Exception {
		if(GameVersion.vals==null) GameVersion.vals = GameVersion.getAllVersions();
		return GameVersion.vals.get(version.toLowerCase());
	}
}
