package com.Richee.Filter;

public enum Sortby {
	DateCreated("created"),
	LastUpdated("updatet"),
	Name("name"),
	Popularity("popularity"),
	TotalDownloads("downloads");
	
	private String f;
	
	Sortby(String f) {
		this.f = f;
	}
	
	public String f() { return this.f; }
}
