package com.Richee.Filter;

public enum Categorys {
	BukkitPlugins,
	AntiGriefingTools,
	ChatReltaed,
	DevTools,
	Economy,
	Fixes,
	Fun,
	General,
	Informational,
	Mechanics,
	Miscelleanous,
	RolePlaying,
	Teleportation,
	WebsiteAdminstration,
	WorldEditingAndManagement,
	WorldGenerators,
	AdminTools;
	
	public String getCategoryName() {
		switch(this) {
		case AdminTools:
			return "/admin-tools";
		case AntiGriefingTools:
			return "/anti-griefing-tools";
		case BukkitPlugins:
			return "";
		case ChatReltaed:
			return "/chat-related";
		case DevTools:
			return "/dev-tools";
		case Economy:
			return "/economy";
		case Fixes:
			return "/fixes";
		case Fun:
			return "/fun";
		case General:
			return "/general";
		case Informational:
			return "/informational";
		case Mechanics:
			return "/mechanics";
		case Miscelleanous:
			return "/miscelleanous";
		case RolePlaying:
			return "/role-playing";
		case Teleportation:
			return "/teleportation";
		case WebsiteAdminstration:
			return "/website-administration";
		case WorldEditingAndManagement:
			return "/world-editing-and-management";
		case WorldGenerators:
			return "/world-generators";
		default:
			return null;
		}
	}
}
