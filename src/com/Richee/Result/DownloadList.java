package com.Richee.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import com.Richee.Search.Version;

@SuppressWarnings({"unused","rawtypes"})
public class DownloadList extends HashMap<String, String> {
	private static final long serialVersionUID = 1L;
	
	private List<String> list = new ArrayList<String>();
	private HashMap<Integer, String> nameList = new HashMap<Integer, String>();
	
	public DownloadList(List<?> pls) {
		
		SwingWorker[] sws = new SwingWorker[pls.size()];
		int i = 0;
		for(Object pl : pls) {
			sws[i] = new SwingWorker<Version, Void>() {
				@Override
				protected Version doInBackground() throws Exception {
					
					String pls = pl.toString();
					Version v = new Version(pls);
					
					return v;
				}
			};
			sws[i].execute();
			i++;
		}
		
		for(i = 6; i>0; i--) {
			try {
				Thread.sleep(1000);
			} catch(Exception ex) {
			}
		}
		i=0;
		
		for(SwingWorker sw : sws) {
			try {
			
				Version v = (Version) sw.get();
				put(v.name(), v.download());
				nameList.put(i, v.name());
				
			} catch (InterruptedException | ExecutionException e) {
				continue;
			}
			i++;
		}
	}
	
	public String getURL(Integer i) {
		return this.get(nameList.get(i));
	}
}
