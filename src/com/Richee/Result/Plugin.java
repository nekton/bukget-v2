package com.Richee.Result;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Plugin {
	Document overview;
	DownloadList dwls;
	Integer id;
	
	public Plugin(SearchResult sr, int pos) throws Exception {
		
		this.dwls = new DownloadList(sr.plugins());
		this.overview = Jsoup.connect(sr.plugins().get(pos).toString()).get();
		this.id = pos;
		
	}
	
	public String dwlUrl() {
		return this.dwls.getURL(this.id);
	}
	
}
