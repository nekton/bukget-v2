package com.Richee.Result;

import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

import org.json.simple.JSONArray;

@SuppressWarnings("unused")
public class SearchResult {
	private static final long serialVersionUID = 1L;
	
	private List<?> plugins = null;
	
	public SearchResult(List<?> list) {
		this.plugins = list;
	}
	
	@SuppressWarnings("unchecked")
	public JSONArray toJSONArray() {
		JSONArray arr = new JSONArray();
		for(Object o : this.plugins) {
			arr.add(o);
		}
		return arr;
	}
	
	public TreeModel getTreeModel(String rootname) {
		
		return new DefaultTreeModel(new DefaultMutableTreeNode(rootname) {
			private static final long serialVersionUID = 1L;
			{
				for(Object o : plugins) {
					add(new DefaultMutableTreeNode(o.toString()));
				}
			}
		});
		
	}
	
	public List<?> plugins() { return this.plugins; } // Returns a bukkit.org link list
}